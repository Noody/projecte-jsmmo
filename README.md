Explicar NodeJS

<! -- Prepare modules -->
npm install
npm install socket.io # Communication
npm install sanitizer # HTML code cleaner on server-side
npm install express # Deliver files
npm install electron # Custom client framework (backend)
npm install crypto # SSL/TLS secure connection (npm built-in)
